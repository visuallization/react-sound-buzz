import React from 'react';

class Sound extends React.Component {

  constructor(props) {
    super(props);
    this.song = new buzz.sound(props.src);
  }

  render() {
    if(this.props.action == "PLAY") {
      this.song.play();
    } else if(this.props.action == "PAUSE") {
      this.song.pause();
    } else if(this.props.action == "STOP") {
      this.song.stop();
    }
    return (null);
  }

}

export default Sound;
