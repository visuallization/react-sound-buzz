# README #

Simple react sound component based on the buzz sound library.

### What is this repository for? ###

* Use this simple react sound component to play audio in your projects.
* Version 0.1

### How do I get set up? ###

* Start your terminal and navigate to the project folder.
* Run "npm install" and then "npm start" in your terminal.
* In your Browser, go to "localhost: 3333"
* You are read to check out the demo.


### How do I use the component? ###


```
#!javascript

import Sound from './yourPathToComponent/Sound';

render () {
return <Sound src="your/path/to/sound.mp3" action="PLAY"/>;
}
```


### Props ###

* src: The path to your audio file.
* action ["PLAY", "PAUSE", "STOP"]: Use one of the 3 values to either play, pause or stop the audio.

### Contribution guidelines ###

* Feel free to contribute

### Who do I talk to? ###

* Florentin Rieger (florentin.rieger@gmail.com)