import React from 'react';
import Sound from './Sound';
import theme from './themes/happy';

class App extends React.Component {

	constructor() {
		super();
		this.state = { action: "STOP" };
		this.handleButtonClickPlay = this.handleButtonClickPlay.bind(this);
		this.handleButtonClickPause = this.handleButtonClickPause.bind(this);
		this.handleButtonClickStop = this.handleButtonClickStop.bind(this);
	}

	render() {
		return (
			<div>
				<button onClick={this.handleButtonClickPlay}>Play</button>
				<button onClick={this.handleButtonClickPause}>Pause</button>
				<button onClick={this.handleButtonClickStop}>Stop</button>
				<Sound src={theme.src} action={this.state.action}></Sound>
			</div>
		);
	}

	handleButtonClickPlay(e) {
		this.setState({ action: "PLAY" });
	}

	handleButtonClickPause(e) {
		this.setState({ action: "PAUSE" });
	}

	handleButtonClickStop(e) {
		this.setState({ action: "STOP" });
	}

}

export default App;
